package com.jcastro.armeriaHibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Usuario {
    private int idUsuario;
    private String nombre;
    private String apellidos;
    private List<Arma> armas;
    private List<Escudo> escudos;
    private List<Juegousuario> juegosusuarios;

    @Id
    @Column(name = "idUsuario")
    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return idUsuario == usuario.idUsuario &&
                Objects.equals(nombre, usuario.nombre) &&
                Objects.equals(apellidos, usuario.apellidos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idUsuario, nombre, apellidos);
    }

    @OneToMany(mappedBy = "usuario")
    public List<Arma> getArmas() {
        return armas;
    }

    public void setArmas(List<Arma> armas) {
        this.armas = armas;
    }

    @OneToMany(mappedBy = "usuario")
    public List<Escudo> getEscudos() {
        return escudos;
    }

    public void setEscudos(List<Escudo> escudos) {
        this.escudos = escudos;
    }

    @OneToMany(mappedBy = "usuario")
    public List<Juegousuario> getJuegosusuarios() {
        return juegosusuarios;
    }

    public void setJuegosusuarios(List<Juegousuario> juegosusuarios) {
        this.juegosusuarios = juegosusuarios;
    }
}
