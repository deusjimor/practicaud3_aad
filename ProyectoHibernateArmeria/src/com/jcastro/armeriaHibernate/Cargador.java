package com.jcastro.armeriaHibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Cargador {
    private int idCargador;
    private int tamaño;
    private int capacidad;
    private Arma arma;
    private List<Bala> balas;

    @Id
    @Column(name = "idCargador")
    public int getIdCargador() {
        return idCargador;
    }

    public void setIdCargador(int idCargador) {
        this.idCargador = idCargador;
    }

    @Basic
    @Column(name = "tamaño")
    public int getTamaño() {
        return tamaño;
    }

    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }

    @Basic
    @Column(name = "capacidad")
    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cargador cargador = (Cargador) o;
        return idCargador == cargador.idCargador &&
                tamaño == cargador.tamaño &&
                capacidad == cargador.capacidad;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCargador, tamaño, capacidad);
    }

    @ManyToOne
    @JoinColumn(name = "idCargador", referencedColumnName = "idCargador", nullable = false)
    public Arma getArma() {
        return arma;
    }

    public void setArma(Arma arma) {
        this.arma = arma;
    }

    @OneToMany(mappedBy = "cargador")
    public List<Bala> getBalas() {
        return balas;
    }

    public void setBalas(List<Bala> balas) {
        this.balas = balas;
    }
}
