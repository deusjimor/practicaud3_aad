package com.jcastro.armeriaHibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Arma {
    private int idArma;
    private String nombre;
    private String tipo;
    private int estadisticas;
    private int porcentaje;
    private Usuario usuario;
    private List<Cargador> cargadores;

    @Id
    @Column(name = "idArma")
    public int getIdArma() {
        return idArma;
    }

    public void setIdArma(int idArma) {
        this.idArma = idArma;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "tipo")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "estadisticas")
    public int getEstadisticas() {
        return estadisticas;
    }

    public void setEstadisticas(int estadisticas) {
        this.estadisticas = estadisticas;
    }

    @Basic
    @Column(name = "porcentaje")
    public int getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(int porcentaje) {
        this.porcentaje = porcentaje;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Arma arma = (Arma) o;
        return idArma == arma.idArma &&
                estadisticas == arma.estadisticas &&
                porcentaje == arma.porcentaje &&
                Objects.equals(nombre, arma.nombre) &&
                Objects.equals(tipo, arma.tipo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idArma, nombre, tipo, estadisticas, porcentaje);
    }

    @ManyToOne
    @JoinColumn(name = "idArma", referencedColumnName = "idArma", nullable = false)
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @OneToMany(mappedBy = "arma")
    public List<Cargador> getCargadores() {
        return cargadores;
    }

    public void setCargadores(List<Cargador> cargadores) {
        this.cargadores = cargadores;
    }
}
