package com.jcastro.armeriaHibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Escudo {
    private int idEscudo;
    private int vida;
    private String calidad;
    private Usuario usuario;

    @Id
    @Column(name = "idEscudo")
    public int getIdEscudo() {
        return idEscudo;
    }

    public void setIdEscudo(int idEscudo) {
        this.idEscudo = idEscudo;
    }

    @Basic
    @Column(name = "vida")
    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    @Basic
    @Column(name = "calidad")
    public String getCalidad() {
        return calidad;
    }

    public void setCalidad(String calidad) {
        this.calidad = calidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Escudo escudo = (Escudo) o;
        return idEscudo == escudo.idEscudo &&
                vida == escudo.vida &&
                Objects.equals(calidad, escudo.calidad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idEscudo, vida, calidad);
    }

    @ManyToOne
    @JoinColumn(name = "idEscudo", referencedColumnName = "idEscudo", nullable = false)
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
