package com.jcastro.armeriaHibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Bala {
    private int idBala;
    private String material;
    private int calibre;
    private Cargador cargador;

    @Id
    @Column(name = "idBala")
    public int getIdBala() {
        return idBala;
    }

    public void setIdBala(int idBala) {
        this.idBala = idBala;
    }

    @Basic
    @Column(name = "material")
    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Basic
    @Column(name = "calibre")
    public int getCalibre() {
        return calibre;
    }

    public void setCalibre(int calibre) {
        this.calibre = calibre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bala bala = (Bala) o;
        return idBala == bala.idBala &&
                calibre == bala.calibre &&
                Objects.equals(material, bala.material);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idBala, material, calibre);
    }

    @ManyToOne
    @JoinColumn(name = "idBala", referencedColumnName = "idBala", nullable = false)
    public Cargador getCargador() {
        return cargador;
    }

    public void setCargador(Cargador cargador) {
        this.cargador = cargador;
    }
}
