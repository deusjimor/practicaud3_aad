package com.jcastro.armeriaHibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Juego {
    private int idJuego;
    private String titulo;
    private int precio;
    private List<Juegousuario> juegosusuarios;

    @Id
    @Column(name = "idJuego")
    public int getIdJuego() {
        return idJuego;
    }

    public void setIdJuego(int idJuego) {
        this.idJuego = idJuego;
    }

    @Basic
    @Column(name = "titulo")
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Basic
    @Column(name = "precio")
    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Juego juego = (Juego) o;
        return idJuego == juego.idJuego &&
                precio == juego.precio &&
                Objects.equals(titulo, juego.titulo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idJuego, titulo, precio);
    }

    @OneToMany(mappedBy = "juego")
    public List<Juegousuario> getJuegosusuarios() {
        return juegosusuarios;
    }

    public void setJuegosusuarios(List<Juegousuario> juegosusuarios) {
        this.juegosusuarios = juegosusuarios;
    }
}
