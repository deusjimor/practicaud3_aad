create database armeria;
use armeria;

--
-- Tabla Juego
--

create table juego(
idJuego int primary key auto_increment,
titulo varchar(50),
precio int
);

--
-- Tabla Usuario
--

create table usuario(
idUsuario int primary key auto_increment,
nombre varchar(50),
apellidos varchar(50),
idEscudo int,
idArma int
);

--
-- Tabla de relaccion Juego_Usuario
--

create table juegoUsuario(
idJuego int,
idUsuario int,
primary key(idJuego, idUsuario),
cantidad int
);

--
-- Tabla Escudo
--

create table escudo(
idEscudo int primary key auto_increment,
vida int,
calidad varchar(50)
);

--
-- Tabla Arma
--

create table arma(
idArma int primary key auto_increment,
nombre varchar(50),
tipo varchar(50),
estadisticas int,
porcentaje int,
idCargador int
);

--
-- Tabla Cargador
--

create table cargador(
idCargador int primary key auto_increment,
tamaño int,
capacidad int,
idBala int
);

--
-- Tabla Bala
--

create table bala(
idBala int primary key auto_increment,
material varchar(10),
calibre int
);

--
-- Foreign keys
--

alter table usuario
add foreign key (idEscudo) references escudo(idEscudo),
add foreign key (idArma) references arma(idArma);

alter table arma
add foreign key (idCargador) references cargador(idCargador);

alter table cargador
add foreign key (idBala) references bala(idBala);

alter table juegoUsuario
add foreign key (idJuego) references juego(idJuego),
add foreign key (idUsuario) references usuario(idUsuario);